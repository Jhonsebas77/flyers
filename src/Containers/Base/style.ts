
import { getComponentStyle } from '../../Helpers/Stylus'
import { Fonts } from '../../Config/Constant'
const styles = getComponentStyle(
    {
        title: {
            fontFamily: Fonts.SEMIBOLD,
            fontSize: 18,
            lineHeight: 24,
            letterSpacing: 0,
            textAlign: 'center',
            color: 'rgba(46, 48, 48,1)',
            marginTop: 13,
            marginBottom: 11
        }
    }
)
export default styles