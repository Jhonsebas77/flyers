import React, { Component } from 'react'
import { Scene, Router, Stack } from 'react-native-router-flux'
import Base from '../Containers/Base/content'
export default class Routes extends Component {
  render() {
    return (
      <Router>
        <Stack key='root'>
          <Scene key='Prueba' component={Base} />
        </Stack>
      </Router>
    )
  }
}