import React from 'react';
import { createIconSet } from 'react-native-vector-icons';
import * as android from '../Assets/json/AlmundoIcons.json';
const AppIcon = createIconSet(android, 'AlmundoFont', 'AlmundoFont.ttf');
export const Icon = ({ iconName, iconSize, iconColor, iconStyle }) => {
    return (React.createElement(AppIcon, { name: iconName, size: iconSize, color: iconColor, style: iconStyle, allowFontScaling: true }));
};
