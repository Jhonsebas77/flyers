import React, { Component } from 'react';
import { Scene, Router, Stack } from 'react-native-router-flux';
import Base from '../Containers/Base/content';
export default class Routes extends Component {
    render() {
        return (React.createElement(Router, null,
            React.createElement(Stack, { key: 'root' },
                React.createElement(Scene, { key: 'Prueba', component: Base }))));
    }
}
