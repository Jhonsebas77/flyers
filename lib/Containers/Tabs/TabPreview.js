import React, { Component } from 'react';
import { Text, View } from 'react-native';
export default class TabPreview extends Component {
    render() {
        return (React.createElement(View, null,
            React.createElement(Text, null, this.props.title)));
    }
}
