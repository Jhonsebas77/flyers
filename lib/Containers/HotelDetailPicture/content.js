import React, { Component } from 'react';
import { Text, View, FlatList, ScrollView } from 'react-native';
import styles from './style';
import Features from '../../Components/Features';
import Options from '../../Components/Options';
import Booking from '../../Components/Booking';
import * as feat from '../../Assets/json/features.json';
import { BoxShadow } from 'react-native-shadow';
import Dash from 'react-native-dash';
import CustomImage from '../../Components/CustomImage';
const badges = feat.featuresBadges;
const text = feat.detailText;
export default class HotelDetailPicture extends Component {
    render() {
        return (React.createElement(View, null,
            React.createElement(CustomImage, { imageSource: require('../../Assets/images/hotel1_black.jpg'), header: 'Habitacion estandar doble, con ba\u00F1o, cocina' }),
            React.createElement(BoxShadow, { setting: styles.shadowFeat },
                React.createElement(FlatList, { horizontal: true, keyExtractor: (x, i) => i.toString(), data: badges, style: styles.flatList, showsHorizontalScrollIndicator: false, renderItem: ({ item }) => React.createElement(Features, { icono: item.icon, nombre: item.name }) })),
            React.createElement(View, { style: styles.scrollViewContainer },
                React.createElement(ScrollView, { showsVerticalScrollIndicator: false },
                    React.createElement(View, { style: styles.paddingContainer },
                        React.createElement(View, { style: styles.containerText },
                            React.createElement(Text, { style: styles.title }, text[0].TituloDescripcion),
                            React.createElement(Text, { style: styles.content, textBreakStrategy: 'simple' }, text[0].Descripcion)),
                        React.createElement(Dash, { style: styles.dashViewer, dashColor: 'rgba(221, 222, 222,1)' }),
                        React.createElement(View, { style: styles.containerInfo },
                            React.createElement(Text, { style: styles.titleInfo }, text[0].TituloInformacion),
                            React.createElement(Text, { style: styles.content, textBreakStrategy: 'simple' }, text[0].Informacion)),
                        React.createElement(Options, null))),
                React.createElement(Booking, null))));
    }
}
