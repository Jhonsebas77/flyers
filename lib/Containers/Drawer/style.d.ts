declare const _default: {
    container: {
        flex: number;
        backgroundColor: string;
    };
    header: {
        width: number;
        height: number;
        paddingTop: number;
        paddingLeft: number;
    };
    imageProfile: {
        width: number;
        height: number;
        backgroundColor: string;
        marginBottom: number;
        borderRadius: number;
    };
    textProfile: {
        fontFamily: string;
        fontSize: number;
        fontStyle: string;
        lineHeight: number;
        letterSpacing: number;
        textAlign: string;
        color: string;
    };
    button: {
        paddingVertical: number;
        paddingLeft: number;
        flexDirection: string;
        width: number;
    };
    buttonWithBorder: {
        borderBottomWidth: number;
        borderBottomColor: string;
    };
    buttonIcon: {
        width: number;
        height: number;
        backgroundColor: string;
        marginRight: number;
    };
    buttonText: {
        width: number;
        height: number;
        fontFamily: string;
        fontSize: number;
        fontStyle: string;
        lineHeight: number;
        letterSpacing: number;
        textAlign: string;
        color: string;
    };
    buttonActive: {};
};
export default _default;
