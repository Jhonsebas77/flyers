import React, { Component } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import style from './style';
import { getComponentStyle } from '../../Helpers/Stylus';
const styles = getComponentStyle(style);
export default class DrawerContent extends Component {
    render() {
        return (React.createElement(View, { style: styles.container },
            React.createElement(LinearGradient, { start: { x: 0.6, y: 0.0 }, end: { x: 1.0, y: 1.0 }, colors: ['#ef5375', '#ea6422'], style: styles.header },
                React.createElement(View, { style: styles.imageProfile }),
                React.createElement(Text, { style: styles.textProfile }, '¡Hola extraño!')),
            React.createElement(TouchableOpacity, { style: [styles.button, styles.buttonWithBorder] },
                React.createElement(View, { style: styles.buttonIcon }),
                React.createElement(Text, { style: styles.buttonText }, 'Iniciar sesión')),
            React.createElement(TouchableOpacity, { style: styles.button },
                React.createElement(View, { style: [styles.buttonIcon, styles.buttonActive] }),
                React.createElement(Text, { style: [styles.buttonText, styles.buttonActive] }, 'Inicio'))));
    }
}
