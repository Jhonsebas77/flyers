import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent',
        borderWidth: 2,
        borderColor: 'red'
    }
});
class DrawerContent extends Component {
    render() {
        return (React.createElement(View, { style: styles.container },
            React.createElement(Text, null, "Drawer Content"),
            React.createElement(TouchableOpacity, { onPress: Actions.closeDrawer },
                React.createElement(Text, null, 'Back'))));
    }
}
export default DrawerContent;
