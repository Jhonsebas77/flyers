import { Fonts } from '../../Config/Constant';
export default {
    container: {
        flex: 1,
        backgroundColor: 'rgba(250, 250, 250, 1)'
    },
    header: {
        width: 304,
        height: 176,
        paddingTop: 50,
        paddingLeft: 15
    },
    imageProfile: {
        width: 64,
        height: 64,
        backgroundColor: 'rgba(255, 255, 255, 0.3)',
        marginBottom: 14,
        borderRadius: 32
    },
    textProfile: {
        fontFamily: Fonts.SEMIBOLD,
        fontSize: 18,
        fontStyle: 'normal',
        lineHeight: 24,
        letterSpacing: 0,
        textAlign: 'left',
        color: 'rgba(255, 255, 255,1)'
    },
    button: {
        paddingVertical: 28,
        paddingLeft: 28,
        flexDirection: 'row',
        width: 304
    },
    buttonWithBorder: {
        borderBottomWidth: 1,
        borderBottomColor: 'rgba(193, 193, 193,1)'
    },
    buttonIcon: {
        width: 16,
        height: 16,
        backgroundColor: 'rgba(159, 162, 162, 1)',
        marginRight: 28
    },
    buttonText: {
        width: 94,
        height: 20,
        fontFamily: Fonts.SEMIBOLD,
        fontSize: 16,
        fontStyle: 'normal',
        lineHeight: 20,
        letterSpacing: 0,
        textAlign: 'left',
        color: 'rgba(46, 48, 48, 1)'
    },
    buttonActive: {}
};
