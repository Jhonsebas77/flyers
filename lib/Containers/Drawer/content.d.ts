/// <reference types="react" />
import { Component } from 'react';
declare class DrawerContent extends Component {
    render(): JSX.Element;
}
export default DrawerContent;
