import React, { Component } from 'react';
import AlmundoRouter from '../Config/Router';
import '../Helpers/Traslations';
export default class App extends Component {
    constructor(props) {
        super(props);
        console.ignoredYellowBox = true;
    }
    render() {
        return (React.createElement(AlmundoRouter, null));
    }
}
