import React, { Component } from 'react';
import { Text, View, FlatList, ScrollView, TouchableOpacity } from 'react-native';
import styles from './style';
import Features from '../../Components/Features';
import Options from '../../Components/Options';
import Booking from '../../Components/Booking';
import * as feat from '../../Assets/json/features.json';
import { Actions } from 'react-native-router-flux';
import { BoxShadow } from 'react-native-shadow';
import Dash from 'react-native-dash';
const badges = feat.featuresBadges;
const text = feat.detailText;
export default class HotelDetail extends Component {
    render() {
        return (React.createElement(View, null,
            React.createElement(BoxShadow, { setting: styles.shadowFeat },
                React.createElement(FlatList, { horizontal: true, keyExtractor: (x, i) => i.toString(), data: badges, showsHorizontalScrollIndicator: false, style: styles.flatList, renderItem: ({ item }) => React.createElement(Features, { icono: item.icon, nombre: item.name }) })),
            React.createElement(View, { style: styles.scrollViewContainer },
                React.createElement(ScrollView, { showsVerticalScrollIndicator: false },
                    React.createElement(View, { style: styles.paddingContainer },
                        React.createElement(View, { style: styles.containerText },
                            React.createElement(Text, { style: styles.title }, text[0].TituloDescripcion),
                            React.createElement(Text, { style: styles.content, textBreakStrategy: 'simple' }, text[0].Descripcion)),
                        React.createElement(Dash, { style: styles.dashViewer, dashColor: 'rgba(221, 222, 222,1)' }),
                        React.createElement(View, { style: styles.containerInfo },
                            React.createElement(Text, { style: styles.titleInfo }, text[0].TituloInformacion),
                            React.createElement(Text, { style: styles.content, textBreakStrategy: 'simple' }, text[0].Informacion)),
                        React.createElement(Options, null),
                        React.createElement(View, { style: { alignSelf: 'center', alignItems: 'center' } },
                            React.createElement(TouchableOpacity, { style: styles.buttonContainer, onPress: () => Actions.DetailPicture() },
                                React.createElement(Text, { style: styles.buttonText }, "Ir otra Vista"))))),
                React.createElement(Booking, null))));
    }
}
