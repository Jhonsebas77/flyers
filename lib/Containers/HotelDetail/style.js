import { getComponentStyle } from '../../Helpers/Stylus';
import { Fonts } from '../../Config/Constant';
const styles = getComponentStyle({
    container: {
        width: 360,
        height: 854,
        backgroundColor: 'rgba(248, 248, 248,0)'
    },
    flatList: {
        backgroundColor: 'rgba(255, 255, 255, 1.0)',
        width: 360,
        height: 96
    },
    title: {
        fontFamily: Fonts.SEMIBOLD,
        fontSize: 18,
        lineHeight: 24,
        letterSpacing: 0,
        textAlign: 'left',
        color: 'rgba(46, 48, 48,1)',
        marginTop: 13,
        marginBottom: 11
    },
    titleInfo: {
        fontFamily: Fonts.SEMIBOLD,
        fontSize: 18,
        lineHeight: 24,
        letterSpacing: 0,
        textAlign: 'left',
        color: 'rgb(46, 48, 48)',
        marginBottom: 11
    },
    content: {
        fontFamily: Fonts.REGULAR,
        fontSize: 16,
        lineHeight: 20,
        letterSpacing: 0,
        textAlign: 'left',
        color: 'rgba(98, 101, 101,1)'
    },
    containerText: {
        marginBottom: 20
    },
    paddingContainer: {
        paddingHorizontal: 8,
        paddingBottom: 41
    },
    containerInfo: {
        marginBottom: 12,
        marginTop: 13
    },
    buttonContainer: {
        width: 144,
        height: 40,
        borderRadius: 4,
        backgroundColor: 'rgba(234, 100, 34,1)',
        marginTop: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonText: {
        fontFamily: Fonts.SEMIBOLD,
        fontSize: 14,
        lineHeight: 20,
        letterSpacing: 0,
        textAlign: 'center',
        color: 'rgb(255, 255, 255)',
        paddingBottom: 9,
        paddingTop: 11
    },
    scrollViewContainer: {
        maxHeight: 450
    },
    shadowFeat: {
        width: 360,
        height: 96,
        color: '#000',
        border: 2,
        radius: 3,
        opacity: 0.1,
        x: 0,
        y: 3,
        style: { marginVertical: 1, marginTop: 15 }
    },
    dashDivider: {
        width: 344,
        height: 1
    }
});
export default styles;
