export const Fonts = {
    BLACK: 'ProximaNova-Black',
    BOLD: 'ProximaNova-Bold',
    BOLDITALIC: 'ProximaNova-BoldIt',
    EXTRABOLD: 'ProximaNova-Extrabold',
    LIGHT: 'ProximaNova-Light',
    LIGHTITALIC: 'ProximaNova-LightItalic',
    REGULAR: 'ProximaNova-Regular',
    REGULARITALIC: 'ProximaNova-RegularItalic',
    SEMIBOLD: 'ProximaNova-Semibold',
    SEMIBOLDITALIC: 'ProximaNova-SemiboldItalic',
    CONDENSEDLIGHT: 'ProximaNovaCond-Light',
    CONDENSEDLIGHTITALIC: 'ProximaNovaCond-LightIt',
    CONDENSEDREGULAR: 'ProximaNovaCond-Regular',
    CONDENSEDREGULARITALIC: 'ProximaNovaCond-RegularIt',
    CONDENSEDSEMIBOLD: 'ProximaNovaCond-Semibold',
    CONDENSEDSEMIBOLDITALIC: 'ProximaNovaCond-SemiboldIt'
};
