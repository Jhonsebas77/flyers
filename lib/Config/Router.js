import React from 'react';
import { Platform, StyleSheet, Text, PixelRatio } from 'react-native';
import { Scene, Router, Reducer, Tabs, Drawer, Stack } from 'react-native-router-flux';
import { Icon } from '../Helpers/Icons';
import DrawerContent from '../Containers/Drawer/';
import TabPreview from '../Containers/Tabs/TabPreview';
const reducerCreate = params => {
    const defaultReducer = new Reducer(params);
    return (state, action) => {
        console.log('ACTION:', action);
        return defaultReducer(state, action);
    };
};
const getSceneStyle = () => ({
    backgroundColor: '#F5FCFF',
    shadowOpacity: 1,
    shadowRadius: 3
});
const prefix = Platform.OS === 'android' ? 'almundo://almundo/' : 'almundo://';
const AlmundoRouter = () => (React.createElement(Router, null,
    React.createElement(Scene, { key: 'root' },
        React.createElement(Drawer, { hideNavBar: true, key: 'drawer', contentComponent: DrawerContent, drawerWidth: 304, drawerPosition: 'left' },
            React.createElement(Scene, { key: 'algo', hideNavBar: true, panHandlers: null },
                React.createElement(Tabs, { key: 'tabbar', swipeEnabled: true, animationEnabled: true, showLabel: true, tabBarStyle: styles.tabBarStyle, activeTintColor: 'white', inactiveTintColor: 'rgba(255, 0, 0, 0.5)', tabBarPosition: 'bottom' },
                    React.createElement(Stack, { key: 'tab1c', title: 'Tab #1', tabBarLabel: 'TAB #1', inactiveBackgroundColor: '#FFF', activeBackgroundColor: '#DDD', iconName: 'back', iconSize: 30, icon: Icon, allowFontScaling: true, initial: true, navigationBarStyle: { backgroundColor: 'green' }, titleStyle: { color: 'white', alignSelf: 'center' } },
                        React.createElement(Scene, { key: 'tab1', component: TabPreview, title: 'Tab #1_1', onRight: () => alert('Right button'), rightTitle: 'Right' })),
                    React.createElement(Stack, { key: 'tab_2', title: 'Tab #2', iconName: 'VR', iconSize: 30, icon: Icon },
                        React.createElement(Scene, { key: 'tab_2_1', component: TabPreview, title: 'Tab #2_1', renderRightButton: () => React.createElement(Text, null, "Right") }))))))));
const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    tabBar: {
        borderTopColor: 'darkgrey',
        borderTopWidth: 1 / PixelRatio.get(),
        backgroundColor: 'ghostwhite',
        opacity: 0.98
    },
    navigationBarStyle: {
        backgroundColor: 'red'
    },
    navigationBarTitleStyle: {
        color: 'white'
    },
    tabBarStyle: {
        backgroundColor: '#eee'
    },
    tabBarSelectedItemStyle: {
        backgroundColor: '#ddd'
    }
});
export default AlmundoRouter;
