export declare const Fonts: {
    BLACK: string;
    BOLD: string;
    BOLDITALIC: string;
    EXTRABOLD: string;
    LIGHT: string;
    LIGHTITALIC: string;
    REGULAR: string;
    REGULARITALIC: string;
    SEMIBOLD: string;
    SEMIBOLDITALIC: string;
    CONDENSEDLIGHT: string;
    CONDENSEDLIGHTITALIC: string;
    CONDENSEDREGULAR: string;
    CONDENSEDREGULARITALIC: string;
    CONDENSEDSEMIBOLD: string;
    CONDENSEDSEMIBOLDITALIC: string;
};
