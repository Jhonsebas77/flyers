/// <reference types="react" />
import { Component } from 'react';
export default class Opciones extends Component<{}> {
    constructor(props: any);
    bntAnimation: any;
    handlerButtonAnimation: (ref: any) => any;
    btnSelect(): void;
    validatePicker(val: any, index: any): void;
    render(): JSX.Element;
}
