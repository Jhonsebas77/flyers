import { getComponentStyle } from '../../Helpers/Stylus';
import { Fonts } from '../../Config/Constant';
const styles = getComponentStyle({
    cardContainer: {
        width: 344,
        height: 247,
        borderRadius: 2,
        backgroundColor: 'rgba(238, 248, 247,1)',
        borderWidth: 1,
        borderColor: 'rgba(23, 158, 164,1)',
        paddingLeft: 16,
        paddingRight: 16,
        paddingTop: 12
    },
    title: {
        fontFamily: Fonts.REGULAR,
        fontSize: 18,
        lineHeight: 24,
        letterSpacing: 0,
        textAlign: 'left',
        color: 'rgb(46, 48, 48)',
        marginBottom: 9
    },
    buttonText: {
        fontFamily: Fonts.SEMIBOLD,
        fontSize: 14,
        lineHeight: 20,
        letterSpacing: 0,
        textAlign: 'center',
        color: 'rgba(255, 255, 255,1)',
        margin: 6
    },
    priceContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    buttonContainer: {
        width: 128,
        height: 32,
        borderRadius: 16,
        backgroundColor: 'rgba(23, 158, 164,1)',
        borderStyle: 'solid',
        borderWidth: 1,
        borderColor: 'rgba(23, 158, 164,1)',
        justifyContent: 'center',
        alignItems: 'center',
        overflow: 'hidden',
        marginTop: 18.8
    },
    titleInfo: {
        fontFamily: Fonts.REGULAR,
        fontSize: 14,
        lineHeight: 16,
        letterSpacing: 0,
        textAlign: 'left',
        color: 'rgba(98, 101, 101,1)',
        marginTop: 13.8
    },
    cancelText: {
        fontFamily: Fonts.REGULAR,
        fontSize: 16,
        lineHeight: 24,
        marginTop: 9.6,
        marginBottom: 13,
        letterSpacing: 0,
        textAlign: 'left',
        color: 'rgba(174, 24, 30,1)'
    },
    optionContainer: {
        borderBottomWidth: 1,
        borderTopWidth: 1,
        borderColor: 'rgba(0, 0, 0,0.1)'
    },
    toggleContainerCredit: {
        flexDirection: 'row'
    },
    toggleContainerHotel: {
        flexDirection: 'row',
        marginLeft: 24
    },
    toggleContainer: {
        flexDirection: 'row'
    },
    textPicker: {
        fontFamily: Fonts.REGULAR,
        fontSize: 14,
        lineHeight: 20,
        letterSpacing: 0,
        textAlign: 'left',
        color: 'rgba(23, 158, 164,1)'
    },
    currency: {
        fontFamily: Fonts.LIGHT,
        fontSize: 16,
        lineHeight: 24,
        letterSpacing: 0,
        textAlign: 'left',
        color: 'rgb(46, 48, 48)'
    },
    value: {
        fontFamily: Fonts.SEMIBOLD,
        fontSize: 20,
        lineHeight: 24,
        letterSpacing: 0,
        textAlign: 'left',
        color: 'rgb(46, 48, 48)'
    },
    circle: {
        width: 50,
        height: 50,
        borderRadius: 50 / 2,
        backgroundColor: 'rgba(255, 255, 255,.3)',
        position: 'absolute',
        alignItems: 'center',
        opacity: 0
    },
    icon: {
        fontSize: 16,
        color: 'rgba(23, 158, 164,1)',
        position: 'absolute',
        left: 10,
        bottom: 4
    },
    iconBadge: {
        fontSize: 15,
        color: 'rgba(23, 158, 164,1)'
    },
    textToggleHotel: {
        fontFamily: Fonts.REGULAR,
        fontSize: 14,
        lineHeight: 16,
        letterSpacing: 0,
        textAlign: 'left',
        color: 'rgb(98, 101, 101)',
        marginBottom: 15,
        marginLeft: 10
    },
    textToggleCredit: {
        fontFamily: Fonts.REGULAR,
        fontSize: 14,
        lineHeight: 16,
        letterSpacing: 0,
        textAlign: 'left',
        color: 'rgb(98, 101, 101)',
        marginBottom: 15,
        marginLeft: 11
    },
    iconPicker: {
        fontSize: 14,
        color: 'rgba(23, 158, 164,1)',
        position: 'absolute',
        left: 17,
        top: 65.5
    }
});
export default styles;
