import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Picker } from 'react-native';
import styles from './style';
import * as feat from '../../Assets/json/features.json';
import { initializableButtonAnimate } from '../../Helpers/Animated';
import * as Animatable from 'react-native-animatable';
import { Icon } from '../../Helpers/Icons';
const options = feat.opciones;
const titleOptions = 'Opciones';
export default class Opciones extends Component {
    constructor(props) {
        super(props);
        this.handlerButtonAnimation = ref => this.bntAnimation = ref;
        initializableButtonAnimate();
        this.state = {
            category: '',
            disable: true
        };
    }
    btnSelect() {
        this.bntAnimation.zoomOut();
    }
    validatePicker(val, index) {
        this.setState({ category: val });
        if (val === 0 || val === null) {
            this.setState({ disable: true });
        }
        else {
            this.setState({ disable: false });
        }
    }
    render() {
        return (React.createElement(View, { style: styles.cardContainer },
            React.createElement(Text, { style: styles.title }, titleOptions),
            React.createElement(Picker, { selectedValue: this.state.category, onValueChange: (itemValue, itemIndex) => this.validatePicker(itemValue, itemIndex) },
                React.createElement(Picker.Item, { label: '        Solo Habitacion', color: 'rgb(23, 158, 164)', value: '1' }),
                React.createElement(Picker.Item, { label: '        Desayuno', color: 'rgb(23, 158, 164)', value: '2' }),
                React.createElement(Picker.Item, { label: '        Pension Completa', color: 'rgb(23, 158, 164)', value: '3' })),
            React.createElement(Icon, { iconName: 'sleep', iconStyle: styles.iconPicker }),
            React.createElement(View, { style: styles.optionContainer },
                React.createElement(Text, { style: styles.cancelText }, options[0].CancelText),
                React.createElement(View, { style: styles.toggleContainer },
                    React.createElement(TouchableOpacity, { style: styles.toggleContainerCredit },
                        React.createElement(Icon, { iconName: 'wallet', iconStyle: [styles.iconBadge, { color: 'rgba(159, 162, 162,1)' }] }),
                        React.createElement(Text, { style: styles.textToggleCredit }, options[0].Toggle1)),
                    React.createElement(TouchableOpacity, { style: styles.toggleContainerHotel },
                        React.createElement(Icon, { iconName: 'cash', iconStyle: [styles.iconBadge, { color: 'rgba(159, 162, 162,1)' }] }),
                        React.createElement(Text, { style: styles.textToggleHotel }, options[0].Toggle2)))),
            React.createElement(View, { style: styles.priceContainer },
                React.createElement(View, null,
                    React.createElement(Text, { style: styles.titleInfo }, 'Por noche por habitación'),
                    React.createElement(Text, { style: styles.currency },
                        'ARS',
                        " ",
                        React.createElement(Text, { style: styles.value }, '200.000.298'))),
                React.createElement(TouchableOpacity, { style: styles.buttonContainer, onPress: () => this.btnSelect(), activeOpacity: 1 },
                    React.createElement(Animatable.View, { ref: this.handlerButtonAnimation, duration: 1500, style: styles.circle }),
                    React.createElement(Text, { style: styles.buttonText }, options[0].Boton)))));
    }
}
