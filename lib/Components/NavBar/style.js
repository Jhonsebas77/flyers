import { getComponentStyle } from '../../Helpers/Stylus';
import { Fonts } from '../../Config/Constant';
const styles = getComponentStyle({
    navBarText: {
        fontFamily: Fonts.SEMIBOLD,
        fontSize: 20,
        lineHeight: 24,
        letterSpacing: 0,
        textAlign: 'left',
        color: 'rgb(255, 255, 255)',
        marginLeft: 40,
        marginTop: 22
    },
    navBar: {
        backgroundColor: 'rgba(234, 100, 34, 1)',
        height: 80,
        width: 360,
        alignItems: 'center',
        alignSelf: 'center',
        flexDirection: 'row'
    },
    btnCancel: {
        marginLeft: 19,
        marginTop: 22,
        color: 'white',
        fontSize: 16
    },
    linearGradient: {
        flex: 1,
        paddingLeft: 15,
        paddingRight: 15,
        borderRadius: 5
    },
    buttonText: {
        fontSize: 18,
        fontFamily: 'Gill Sans',
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
        backgroundColor: 'transparent'
    },
    shadowNavBar: {
        width: 360,
        height: 80,
        color: '#000',
        border: 2,
        radius: 3,
        opacity: 0.1,
        x: 0,
        y: 3
    }
});
export default styles;
