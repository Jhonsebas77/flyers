import React, { Component } from 'react';
import { Text, Alert, TouchableOpacity, StatusBar } from 'react-native';
import styles from './style';
import LinearGradient from 'react-native-linear-gradient';
import { BoxShadow } from 'react-native-shadow';
import { Icon } from '../../Helpers/Icons';
import { View } from 'react-native-animatable';
export default class NavBar extends Component {
    render() {
        return (React.createElement(View, null,
            React.createElement(StatusBar, { translucent: true, backgroundColor: 'rgba(0,0,0,0.1)' }),
            React.createElement(BoxShadow, { setting: styles.shadowNavBar },
                React.createElement(LinearGradient, { colors: ['rgba(239,83,117,1)', 'rgba(234,100,34,1)'], style: styles.navBar, start: { x: 0.0, y: 0.0 }, end: { x: 0.9, y: 1.0 } },
                    React.createElement(TouchableOpacity, { onPress: () => Alert.alert('Hola Boton') },
                        React.createElement(Icon, { iconName: 'close', iconStyle: styles.btnCancel })),
                    React.createElement(Text, { style: styles.navBarText }, "Habitaci\u00F3n deluxe")))));
    }
}
