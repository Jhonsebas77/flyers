import { getComponentStyle } from '../../Helpers/Stylus';
import { Fonts } from '../../Config/Constant';
const styles = getComponentStyle({
    overlayHeader: {
        fontFamily: Fonts.SEMIBOLD,
        fontSize: 20,
        lineHeight: 24,
        letterSpacing: 0,
        textAlign: 'left',
        color: 'rgb(255, 255, 255)',
        maxWidth: 250,
        marginTop: 39,
        marginLeft: 29
    },
    btnCancel: {
        color: 'white',
        fontSize: 20,
        marginTop: 40,
        marginLeft: 15
    },
    image: {
        width: 360,
        height: 224,
        flexDirection: 'row'
    },
    iconGallery: {
        marginLeft: 8,
        fontSize: 15,
        marginTop: 4
    },
    textImage: {
        fontFamily: Fonts.SEMIBOLD,
        fontSize: 14,
        lineHeight: 24,
        letterSpacing: 0,
        textAlign: 'left',
        color: 'rgb(255, 255, 255)'
    },
    miniGalery: {
        position: 'absolute',
        flexDirection: 'row',
        width: 60,
        height: 36,
        marginLeft: 291,
        marginTop: 192
    }
});
export default styles;
