import React from 'react';
import { Text, ImageBackground, TouchableOpacity, View } from 'react-native';
import styles from './style';
import { Actions } from 'react-native-router-flux';
import { Icon } from '../../Helpers/Icons';
export default ({ header, imageSource }) => {
    return (React.createElement(ImageBackground, { source: imageSource, style: styles.image },
        React.createElement(TouchableOpacity, { onPress: () => Actions.pop() },
            React.createElement(Icon, { iconName: 'close', iconStyle: [styles.btnCancel, { color: 'rgba(255, 255, 255,1)' }] })),
        React.createElement(Text, { ellipsizeMode: 'tail', numberOfLines: 1, style: styles.overlayHeader },
            " ",
            header),
        React.createElement(View, { style: styles.miniGalery },
            React.createElement(Text, { style: styles.textImage }, " 1 | 26"),
            React.createElement(Icon, { iconName: 'camera', iconStyle: [styles.iconGallery, { color: 'rgba(255, 255, 255,1)' }] }))));
};
