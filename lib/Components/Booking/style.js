import { getComponentStyle } from '../../Helpers/Stylus';
import { Fonts } from '../../Config/Constant';
const styles = getComponentStyle({
    cardContainer: {
        width: 360,
        height: 71,
        backgroundColor: 'rgba(255, 255, 255,1)',
        paddingRight: 15
    },
    buttonText: {
        fontFamily: Fonts.SEMIBOLD,
        fontSize: 14,
        lineHeight: 20,
        letterSpacing: 0,
        textAlign: 'center',
        color: 'rgb(255, 255, 255)',
        paddingBottom: 9,
        paddingTop: 11
    },
    priceContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    buttonContainer: {
        width: 144,
        height: 40,
        borderRadius: 4,
        backgroundColor: 'rgba(234, 100, 34,1)',
        marginVertical: 16,
        justifyContent: 'center',
        alignItems: 'center',
        overflow: 'hidden'
    },
    titleInfo: {
        fontFamily: Fonts.REGULAR,
        fontSize: 12,
        lineHeight: 16,
        letterSpacing: 0,
        textAlign: 'left',
        color: 'rgb(69, 72, 72)'
    },
    currency: {
        fontFamily: Fonts.LIGHT,
        fontSize: 16,
        lineHeight: 24,
        letterSpacing: 0,
        textAlign: 'left',
        color: 'rgb(46, 48, 48)'
    },
    value: {
        fontFamily: Fonts.SEMIBOLD,
        fontSize: 20,
        lineHeight: 24,
        letterSpacing: 0,
        textAlign: 'left',
        color: 'rgb(46, 48, 48)'
    },
    valueContainer: {
        marginLeft: 18,
        paddingTop: 16
    },
    circle: {
        width: 50,
        height: 50,
        borderRadius: 50 / 2,
        backgroundColor: 'rgba(255, 255, 255,.3)',
        position: 'absolute',
        alignItems: 'center',
        opacity: 0
    },
    shadowBooking: {
        width: 360,
        height: 71,
        color: '#000',
        border: 2,
        radius: 3,
        opacity: 0.1,
        x: 0,
        y: -3,
        style: { marginVertical: 1 }
    }
});
export default styles;
