import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import styles from './style';
import { initializableButtonAnimate } from '../../Helpers/Animated';
import * as Animatable from 'react-native-animatable';
import { BoxShadow } from 'react-native-shadow';
export default () => {
    initializableButtonAnimate();
    let bntAnimation;
    const handlerButtonAnimation = ref => bntAnimation = ref;
    function btnBooking() { bntAnimation.zoomOut(); }
    return (React.createElement(BoxShadow, { setting: styles.shadowBooking },
        React.createElement(View, { style: styles.cardContainer },
            React.createElement(View, { style: styles.priceContainer },
                React.createElement(View, { style: styles.valueContainer },
                    React.createElement(Text, { style: styles.titleInfo }, 'Tarifa total en 1 cuota'),
                    React.createElement(Text, { style: styles.currency },
                        'ARS',
                        " ",
                        React.createElement(Text, { style: styles.value }, '999.225.000'))),
                React.createElement(View, null,
                    React.createElement(TouchableOpacity, { style: styles.buttonContainer, onPress: () => btnBooking(), activeOpacity: 1 },
                        React.createElement(Animatable.View, { ref: handlerButtonAnimation, duration: 1500, style: styles.circle }),
                        React.createElement(Text, { style: styles.buttonText }, 'RESERVAR')))))));
};
