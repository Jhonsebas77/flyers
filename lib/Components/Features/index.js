import React from 'react';
import { View, Text } from 'react-native';
import styles from './style';
import { Icon } from '../../Helpers/Icons';
export default ({ icono, nombre }) => {
    return (React.createElement(View, { style: styles.containerItem },
        React.createElement(Icon, { iconName: icono, iconStyle: styles.icon }),
        React.createElement(Text, { style: styles.textFeature, ellipsizeMode: 'tail', numberOfLines: 2 },
            " ",
            nombre,
            " ")));
};
