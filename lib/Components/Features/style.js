import { getComponentStyle } from '../../Helpers/Stylus';
import { Fonts } from '../../Config/Constant';
const styles = getComponentStyle({
    containerItem: {
        paddingTop: 28.6,
        alignItems: 'center',
        width: 78
    },
    textFeature: {
        fontFamily: Fonts.REGULAR,
        fontSize: 10,
        lineHeight: 12,
        letterSpacing: 0,
        textAlign: 'center',
        color: 'rgb(98, 101, 101)',
        maxWidth: 78
    },
    icon: {
        color: 'rgba(159, 162, 162,1)',
        fontSize: 22,
        marginBottom: 10.9
    }
});
export default styles;
